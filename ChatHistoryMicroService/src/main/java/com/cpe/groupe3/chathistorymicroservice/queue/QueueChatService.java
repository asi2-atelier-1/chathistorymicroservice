package com.cpe.groupe3.chathistorymicroservice.queue;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;

import com.cpe.groupe3.chathistorymicroservice.dto.ChatHistoryDTO;
import com.cpe.groupe3.chathistorymicroservice.mapper.ChatHistoryMapper;
import com.cpe.groupe3.chathistorymicroservice.repository.ChatHistoryRepository;
import com.cpe.groupe3.usermicroservicepublic.consumer.UserRestConsumer;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class QueueChatService {
  @Autowired
  ChatHistoryRepository chatHistoryRepository;
  @Autowired
  UserRestConsumer userRestConsumer;

  @JmsListener(destination = "${queue.name}")
  public void dequeue(String message) {
    ChatHistoryDTO cHD = new ChatHistoryDTO();
    try {
      cHD = new ObjectMapper().readValue(message, ChatHistoryDTO.class);
    } catch (JsonProcessingException e) {
      System.out.println("Couldn't parse the given object " + message + ", Exception : " + e);
    }

    try {
      // TODO : Should this be here ?
      try {
        userRestConsumer.get(cHD.getSrcId().toString());			
        userRestConsumer.get(cHD.getDestId().toString());				
      }
      catch (Exception e) {
        System.out.println("User id:"+cHD.getSrcId()+" or User id:"+cHD.getSrcId()+", does not exist");
      }	
      chatHistoryRepository.save(ChatHistoryMapper.fromDtoToModel(cHD));
    } catch (Exception e) {
      System.out.println(e);
    }

    System.out.println("[BUSLISTENER] [CHANNEL RESULT_BUS_MNG] RECEIVED String MSG=[" + message + "]");
  }
}