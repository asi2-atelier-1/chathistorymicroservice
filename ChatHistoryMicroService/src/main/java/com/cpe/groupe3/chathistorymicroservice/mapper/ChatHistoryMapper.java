package com.cpe.groupe3.chathistorymicroservice.mapper;

import com.cpe.groupe3.chathistorymicroservice.dto.ChatHistoryDTO;
import com.cpe.groupe3.chathistorymicroservice.model.ChatHistoryModel;

public class ChatHistoryMapper {
	public static ChatHistoryDTO fromModelToDto(ChatHistoryModel cHM) {
		ChatHistoryDTO cHD = new ChatHistoryDTO();
		cHD.setDestId(cHM.getDestId());
		cHD.setId(cHM.getId());
		cHD.setSrcId(cHM.getSrcId());
		cHD.setText(cHM.getText());
		cHD.setTime(cHM.getTime());
		return cHD;
	}
	
	public static ChatHistoryModel fromDtoToModel(ChatHistoryDTO cHD) {
		ChatHistoryModel cHM = new ChatHistoryModel();
		cHM.setDestId(cHD.getDestId());
		cHM.setId(cHD.getId());
		cHM.setSrcId(cHD.getSrcId());
		cHM.setText(cHD.getText());
		cHM.setTime(cHD.getTime());
		return cHM;
	}
}
