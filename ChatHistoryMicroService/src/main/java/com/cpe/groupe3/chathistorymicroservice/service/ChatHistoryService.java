package com.cpe.groupe3.chathistorymicroservice.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import com.cpe.groupe3.chathistorymicroservice.dto.ChatHistoryDTO;
import com.cpe.groupe3.chathistorymicroservice.mapper.ChatHistoryMapper;
import com.cpe.groupe3.chathistorymicroservice.model.ChatHistoryModel;
import com.cpe.groupe3.chathistorymicroservice.queue.QueueChatService;
import com.cpe.groupe3.chathistorymicroservice.repository.ChatHistoryRepository;
import com.cpe.groupe3.usermicroservicepublic.consumer.UserRestConsumer;

@Service
public class ChatHistoryService extends QueueChatService {
	@Autowired
	private UserRestConsumer userRestConsumer;
	@Autowired
	private ChatHistoryRepository chatHistoryRepository;
	
	public List<ChatHistoryDTO> getAll() {
		List<ChatHistoryDTO> listCHD = new ArrayList<ChatHistoryDTO>();
		for(ChatHistoryModel cHM : chatHistoryRepository.findAll()){
			listCHD.add(ChatHistoryMapper.fromModelToDto(cHM));
		}
		return listCHD;
	}
	
	public List<ChatHistoryDTO> getAllBySrcId(Integer srcId) {
		List<ChatHistoryDTO> listCHD = new ArrayList<ChatHistoryDTO>();

		try {
			userRestConsumer.get(srcId.toString());				
		}
		catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User id:"+srcId+", does not exist",null);
		}	

		for(ChatHistoryModel cHM : chatHistoryRepository.findBySrcId(srcId)){
			listCHD.add(ChatHistoryMapper.fromModelToDto(cHM));
		}
		return listCHD;
	}
	
	public List<ChatHistoryDTO> getAllByDestId(Integer destId) {
		List<ChatHistoryDTO> listCHD = new ArrayList<ChatHistoryDTO>();

		try {
			userRestConsumer.get(destId.toString());				
		}
		catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User id:"+destId+", does not exist",null);
		}	

		for(ChatHistoryModel cHM : chatHistoryRepository.findByDestId(destId)){
			listCHD.add(ChatHistoryMapper.fromModelToDto(cHM));
		}
		return listCHD;
	}
}