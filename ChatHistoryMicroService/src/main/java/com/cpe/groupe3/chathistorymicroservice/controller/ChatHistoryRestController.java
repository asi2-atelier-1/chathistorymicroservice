package com.cpe.groupe3.chathistorymicroservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.cpe.groupe3.chathistorymicroservice.dto.ChatHistoryDTO;
import com.cpe.groupe3.chathistorymicroservice.service.ChatHistoryService;

//ONLY FOR TEST NEED ALSO TO ALLOW CROOS ORIGIN ON WEB BROWSER SIDE
@CrossOrigin
@RestController
public class ChatHistoryRestController {
	@Autowired
	ChatHistoryService chatHistoryService;
	
    @RequestMapping(method = RequestMethod.GET, value = "/chathistory")
	public List<ChatHistoryDTO> getAll(){
		return chatHistoryService.getAll();
	}
    
    @RequestMapping(method = RequestMethod.GET, value = "/chathistory/src/{srcId}")
	public List<ChatHistoryDTO> getAllBySrcId(@PathVariable Integer srcId){
		try{
			return chatHistoryService.getAllBySrcId(srcId);
		} catch(Exception e) {
			throw(e);
		}

	}
    
    @RequestMapping(method = RequestMethod.GET, value = "/chathistory/dest/{destId}")
	public List<ChatHistoryDTO> getAllByDestId(@PathVariable Integer destId){
		try{
			return chatHistoryService.getAllByDestId(destId);
		} catch(Exception e) {
			throw(e);
		}
	}
}
