package com.cpe.groupe3.chathistorymicroservice.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.cpe.groupe3.chathistorymicroservice.model.ChatHistoryModel;

@Repository
public interface ChatHistoryRepository extends CrudRepository<ChatHistoryModel, Integer>{
	public List<ChatHistoryModel> findBySrcId(Integer srcId);
	public List<ChatHistoryModel> findByDestId(Integer destId);
}
