package com.cpe.groupe3.chathistorymicroservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;

@SpringBootApplication
@ComponentScan(basePackages = { "com.cpe.groupe3.usermicroservicepublic", "com.cpe.groupe3.commonmicroservice" })
@ComponentScan
@OpenAPIDefinition(info = @Info(title = "Chat History MicroService Rest Api", version = "1.0", description = "Information about the Chat History MicroService APi and how to interact with"))
public class ChatHistoryMicroService {
	public static void main(String[] args) {
		SpringApplication.run(ChatHistoryMicroService.class, args);
	}
}

